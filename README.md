# Simple Server Monitoring Demo

## Commands:

Start server -  `node server.js`

Send CPU usage data over TCP (works on CentOS and requires [netcat](http://netcat.sourceforge.net/)) - 

`while true; do top -b -n2 -d 00.20 |grep Cpu|tail -1 | awk -F ":" '{ print $2 }' | cut -d, -f1 | cut -d'%' -f1 | nc 127.0.0.1 1337; done `

Simulate Load - `while :; do : ; done`

## Live Demo

[Demo](http://107.170.112.187:4000/app/#/monitor)

