var net = require('net');
var faye = require('faye');
/**
* Module dependencies.
*/

var bayeux = new faye.NodeAdapter({mount: '/faye', timeout: 45});

var express = require('express');

// Path to our public directory
var pub = __dirname + '/public';

// setup middleware
var app = express();
app.use(express.static(pub));
app.use(app.router);

app.get('/', function(req, res){
  res.send("200");
});

//Attach server to Faye
bayeux.attach(app.listen(4000));


console.log('Express app started on port 4000');


var server = net.createServer(function (socket) {
  socket.on('data', function(data) {
		var usage = data.toString();         	
    //Send CPU usage data to browser
    bayeux.getClient().publish('/test', {
  		usage:       usage,
  	});
  });
});

//Listen for CPU usage data
server.listen(1337, '127.0.0.1');
